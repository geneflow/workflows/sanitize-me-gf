===========================================
SanitizeMe: Remove Host Sequence from Reads
===========================================

Overview
========

This workflow removes host sequence from reads using Minimap2 and SAMTools. 

Requirements
------------

Workflow steps require that a container system (Singularity or Docker) is available, or that each required tool (Minimap2 and SAMTools) are available in the environment. 

Inputs
------

The workflow requires the following inputs.

1. input: Directory containing sequence files.

2. reference: Reference sequence of the host.

Parameters
----------

Workflow parameters can be specified, but default values are used otherwise.

1. threads: Number of CPU threads to use for alignment. Default: 4

2. minimap2_preset: Preset alignment options for minimap2 (map-ont, map-pb, asm20, or sr). Default: map-ont

Detailed Installation Instructions
==================================

Run Instructions
================

