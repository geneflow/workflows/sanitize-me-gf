%YAML 1.1
---
gfVersion: v1.0
class: workflow

# metadata
name: SanitizeMe
description: Remove host sequence with Minimap2 and SAMTools
documentation_uri: https://gitlab.com/geneflow/workflows/sanitize-me-gf.git
repo_uri: https://gitlab.com/geneflow/workflows/sanitize-me-gf.git
version: '0.2'

final_output:
- bam_to_fastq

# inputs
inputs:
  input:
    label: Input Directory
    description: Directory containing sequence files
    type: Directory
    default: /input/files
    enable: true
    visible: true

  reference:
    label: Reference Sequence
    description: Reference sequence of the host
    type: File
    default: /input/reference
    enable: true
    visible: true

# parameters
parameters:
  threads:
    label: Threads
    description: Number of CPU threads
    type: int
    default: 4
    enable: true
    visible: true

  minimap2_preset:
    label: Minimap2 Preset
    description: Preset options for Minimap2
    type: string
    default: map-ont
    enable: true
    visible: true

# steps
steps:
  align:
    app: apps/minimap2-gf/app.yaml
    depend: []
    map:
      uri: '{workflow->input}'
      regex: (.*)\.((fasta|fa|fastq|fq)(|\.gz))$
    template:
      input: '{workflow->input}/{1}.{2}'
      reference: '{workflow->reference}'
      preset: '{workflow->minimap2_preset}'
      threads: '{workflow->threads}'
      output: '{1}.sam'

  filter_flags:
    app: apps/samtools-filter-flags-gf/app.yaml
    depend: [ 'align' ]
    map:
      uri: '{align->output}'
      regex: ^(?!_log)(.*).sam
    template:
      input: '{align->output}/{1}.sam'
      include_reads_all: 4
      output: '{1}.bam'

  bam_to_fastq:
    app: apps/samtools-bam-to-fastq-gf/app.yaml
    depend: [ 'filter_flags' ]
    map:
      uri: '{filter_flags->output}'
      regex: ^(?!_log)(.*).bam
    template:
      input: '{filter_flags->output}/{1}.bam'
      output: '{1}.fastq'
...
